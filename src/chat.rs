use gtk;
use gtk::{ButtonExt,EntryExt};

use ui::ui::UI;

#[allow(dead_code)]
pub struct Chat {
	text_input: gtk::Entry,
	text_send: gtk::Button,
	chat_list: gtk::ListBox,
}

impl Chat {
	pub fn new(ui: &mut UI) -> Self {
	
		let _text_input = ui.text_input.clone();
		let _chat_list = ui.chat_list.clone();
		
		ui.text_send.connect_clicked(move |_| {
			let text = match _text_input.get_text() {
				Some(some) => some,
				None => String::new()
			};
			
			_text_input.set_text("");
			
			let label = gtk::Label::new(Some(text.as_str()));

			_chat_list.insert(&label, 0);
			
			
			
			println!("text_send! '{}'", text);
		});
		
		let _text_send = ui.text_send.clone();
		ui.text_input.connect_activate(move |_| {
			_text_send.clicked();
		});
	
		Chat {
			text_input: ui.text_input.clone(),
			text_send: ui.text_send.clone(),
			chat_list: ui.chat_list.clone(),
		}
	}
}
