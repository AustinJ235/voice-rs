use std::collections::VecDeque;
use std::sync::Arc;
use std::thread;
use crossbeam::sync::MsQueue;
use parking_lot::Mutex;

const JITTER_BUFFER_SIZE: usize = 10; // Needs to be at least amoung+1 or a panic will happen

pub struct In {
	in_queue: Arc<MsQueue<(u16, Vec<i16>)>>,
	out_queue: Arc<MsQueue<Vec<i16>>>,
}

impl In {
	pub fn new(seg_size: usize, amoung: usize) -> In {
		let in_queue = Arc::new(MsQueue::new());
		let out_queue = Arc::new(MsQueue::new());
		let _seg_size = seg_size.clone();
		let _in_queue = in_queue.clone();
		let _out_queue = out_queue.clone();
		let inner_queue_: Arc<Mutex<VecDeque<Option<Vec<i16>>>>> = Arc::new(Mutex::new(VecDeque::new()));
		let _inner_queue_ = inner_queue_.clone();
		let _amoung = amoung.clone();
		
		thread::spawn(move || {
			let out_queue = _out_queue;
			let inner_queue_ = _inner_queue_;
			let amoung = _amoung;
			let seg_size = _seg_size;
			let mut null_frag = Vec::new();
			null_frag.resize(seg_size, i16::max_value());
			let mut last_val = None;
			let mut next_frame = Vec::new();
			next_frame.resize(seg_size, 0);
			
			'master: loop {
				let mut inner_queue = inner_queue_.lock();
				
				{
					if inner_queue.len() < amoung+1 {
						continue;
					}
				
					for i in 0..(amoung+1) {
						if inner_queue[i].is_none() {
							if inner_queue.len() < JITTER_BUFFER_SIZE {	
								continue 'master;
							} else {
								inner_queue[i] = Some(null_frag.clone());
							}
						}
					}
					
					let mut out = next_frame.split_off(0);
					let mut first_some = Vec::new();
				
					for i in (0..amoung).rev() {
						first_some.push(inner_queue[i+1].as_ref().unwrap());
					} for i in 0..seg_size {
						next_frame.push(first_some[(i%amoung)][i].clone());
					}
					
					if out[0] == i16::max_value() {
						if last_val.is_some() {
							let last_val = last_val.as_ref().unwrap();
							let mut stop_at = 0;
							
							for i in 0..seg_size {
								if out[i] != i16::max_value() {
									stop_at = i;
									break;
								}
							}
							
							let step = f32::floor((out[stop_at] as f32 - *last_val as f32) / stop_at as f32) as i16;
							
							for i in 0..stop_at {
								out[i] = *last_val + (step * i as i16);
							}
						} else {
							out[0] = 0;
						}	
					}
					
					if out[seg_size-1] == i16::max_value() {
						let mut next_val = 0;
						let mut next_dist = 1;
						let mut back_val = 0;
						let mut back_dist = 1;
						
						for i in 0..seg_size {
							if next_frame[i] != i16::max_value() {
								next_val = next_frame[i].clone();
								break;
							} next_dist += 1;
						}
						
						for i in (0..seg_size-1).rev() {
							if out[i] != i16::max_value() {
								back_val = out[i].clone();
								break;
							} back_dist += 1;
						}
						
						let total_dist = next_dist + back_dist;
						let step = f32::floor((next_val as f32 - back_val as f32) / total_dist as f32) as i16;
						let start_at = seg_size-back_dist;
						let end_at = seg_size-1+next_dist;
						let mut mul = 1_i16;
						
						for i in start_at..end_at {
							if i < seg_size {
								out[i] = step*mul;
							} else {
								next_frame[i-seg_size] = step*mul;
							} mul += 1;
						}
					}
					
					for i in 1..(seg_size-1) {
						if out[i] == i16::max_value() {
							let mut next_val = 0;
							let mut next_dist = 1;
							let back_val = out[i-1].clone();
							
							for j in i..seg_size {
								if out[j] != i16::max_value() {
									next_val = out[j].clone();
									break;
								} next_dist += 1;
							}
							
							let step = f32::floor((next_val as f32 - back_val as f32) / next_dist as f32) as i16;
							
							for j in 0..next_dist-1 {
								out[i+j] = step * j as i16;
							}
						}
					}
				
					last_val = Some(out[seg_size-1].clone());
					out_queue.push(out);
				}
				
				inner_queue.pop_front();
			}
		});
		
		let _inner_queue_ = inner_queue_.clone();
		
		thread::spawn(move || {
			let in_queue = _in_queue;
			let inner_queue_ = _inner_queue_;
			let mut last_seq = 0_u16;
			
			loop {
				let (seq, seg): (u16, Vec<i16>) = in_queue.pop();
				let mut inner_queue = inner_queue_.lock();
				let seq_diff: i32 = seq as i32 - last_seq as i32;
				
				if seq <= last_seq {
					let index = (inner_queue.len() as i32 - 1_i32) - (last_seq as i32 - seq as i32);
					
					if index >= 0 {
						inner_queue[index as usize] = Some(seg);
						println!("Out of Order packet! Adding");
					} else {
						println!("Out of Order Packet! Dropping");
					}
					
					continue;
				}
				
				last_seq = seq;
				
				if seq_diff > 1 && seq_diff < JITTER_BUFFER_SIZE as i32 {
					for _ in 0..(seq_diff-1) {
						inner_queue.push_back(None);
					} println!("Lost {} Packets", seq_diff-1);
				} else if seq_diff >= JITTER_BUFFER_SIZE as i32 {
					inner_queue.clear();
				}
				
				inner_queue.push_back(Some(seg));
			}
		});
		
		In {
			in_queue: in_queue,
			out_queue: out_queue,
		}
	}
	
	pub fn push(&self, data: (u16, Vec<i16>)) {
		self.in_queue.push(data);
	}
	
	pub fn try_pop(&self) -> Option<Vec<i16>> {
		self.out_queue.try_pop()
	}
	
	pub fn pop(&self) -> Vec<i16> {
		self.out_queue.pop()
	}
}

pub struct Out {
	in_queue: Arc<MsQueue<Vec<i16>>>,
	out_queue: Arc<MsQueue<(u16, Vec<i16>)>>,
}

impl Out {
	pub fn new(seg_size: usize, amoung: usize) -> Out {
		let in_queue = Arc::new(MsQueue::new());
		let out_queue = Arc::new(MsQueue::new());
		let _seg_size = seg_size.clone();
		let _in_queue = in_queue.clone();
		let _out_queue = out_queue.clone();
		
		let mut empty = Vec::new();
		empty.resize(seg_size, 0);
		
		for _ in 0..amoung {
			in_queue.push(empty.clone());
		}
		
		thread::spawn(move || {
			let seg_size = _seg_size;
			let in_queue = _in_queue;
			let out_queue = _out_queue;
			let mut buffer: VecDeque<Vec<i16>> = VecDeque::new();
			let mut seq = 0_u16;
			
			loop {
				let segment: Vec<i16> = in_queue.pop();
				
				if segment.len() != seg_size {
					println!("ERROR: net::Out receive incorrect sized segment!");
					continue;
				} if buffer.len() == amoung {
					buffer.push_back(segment);
					buffer.pop_front();
				} else {
					buffer.push_back(segment);
					continue;
				}
				
				let mut out = Vec::with_capacity(seg_size);
				
				for i in 0..seg_size {
					out.push(buffer[i%amoung][i].clone());
				}
				
				seq = seq.wrapping_add(1);
				out_queue.push((seq, out));
			}
		});
		
		Out {
			in_queue: in_queue,
			out_queue: out_queue,
		}
	}
	
	pub fn push(&self, data: Vec<i16>) {
		self.in_queue.push(data);
	}
	
	pub fn try_pop(&self) -> Option<(u16, Vec<i16>)> {
		self.out_queue.try_pop()
	}
	
	pub fn pop(&self) -> (u16, Vec<i16>) {
		self.out_queue.pop()
	}
}
