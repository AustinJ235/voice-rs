use std::net::UdpSocket;
use std::sync::mpsc;
use std::sync::Arc;
use std::thread;
use rand;
use std::thread::sleep;
use std::time::{Instant,Duration};
use net::stream;
use bincode::{self,serialize, deserialize};
use parking_lot::Mutex;

const SEG_SPLIT: usize = 5;

pub struct Receiver {

}

#[derive(Serialize,Deserialize)]
struct StreamPacket {
	pub seq: u16,
	pub data: Vec<i16>,
}

impl Receiver {
	pub fn new(write_to: Arc<stream::In>, listen_on: &str) -> Result<Self, String> {
		let socket = match UdpSocket::bind(listen_on) {
			Ok(ok) => ok,
			Err(e) => return Err(format!("Failed to open socket, {}", e))
		};
		
		thread::spawn(move || {
			let mut buffer = [0; 3000];
			
			loop {
				if let Ok((len, _)) = socket.recv_from(&mut buffer) {
					let mut buffer_vec = buffer.to_vec();
					buffer_vec.truncate(len);
					let _write_to = write_to.clone();
				
					thread::spawn(move || {
						/*if rand::random::<u8>() < 1 {
							return;
						} sleep(Duration::from_millis(100 + (rand::random::<u8>()/10) as u64));*/
			
						let packet = deserialize::<StreamPacket>(buffer_vec.as_slice()).unwrap();
						_write_to.push((packet.seq, packet.data));
					});
				}
			}
		});
		
		Ok(Receiver {

		})
	}
}

pub struct UdpSender {
	buffer_r: mpsc::Receiver<Vec<i16>>,
	upload_speed_: Arc<Mutex<u64>>,
}

impl UdpSender {
	pub fn new(upload_speed_: Arc<Mutex<u64>>) -> (Self, mpsc::Sender<Vec<i16>>) {
		let (buffer_s, buffer_r) = mpsc::channel();
	
		(UdpSender {
			buffer_r: buffer_r,
			upload_speed_: upload_speed_,
		}, buffer_s)
	}
	
	pub fn send(&self, listen_on: &str, send_to: &str, transmit_: Arc<Mutex<bool>>) {
		let socket = match UdpSocket::bind(listen_on) {
			Ok(ok) => ok,
			Err(e) => {
				println!("{}", e);
				return;
			}
		};
		
		let out_stream = stream::Out::new(960, SEG_SPLIT);
		
		for buffer in self.buffer_r.iter() {
			out_stream.push(buffer);
			let (seq, frag) = out_stream.pop();
			let packet = StreamPacket {
				seq: seq,
				data: frag,
			};
			
			let data = serialize(&packet, bincode::Infinite).unwrap();
			
			if *transmit_.lock() == false {
				continue;
			}
			
			*self.upload_speed_.lock() += data.len() as u64;
			
			if let Err(e) = socket.send_to(data.as_slice(), send_to) {
				println!("Failed to send packet, {}", e);
				continue;
			}
		}
	}
}
