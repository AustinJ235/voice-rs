extern crate gtk;
extern crate gdk;
extern crate alsa;
extern crate parking_lot;
extern crate serde;
extern crate bincode;
#[macro_use]
extern crate serde_derive;
extern crate rand;
extern crate crossbeam;
extern crate libpulse_sys;
extern crate opus;

pub mod ui;
pub mod audio;
pub mod net;
pub mod chat;

use ui::ui::UI;
use audio::input::Input;
use audio::output::Output;
use std::sync::Arc;
use std::thread;
use net::udp::Receiver;
use parking_lot::Mutex;
use gtk::{WidgetExt,MenuItemExt};

#[allow(unused_variables)]
fn main() {
	let mut ui = UI::init();
	let _ = Arc::new(chat::Chat::new(&mut ui));
	let output = Arc::new(Output::new(ui.level_output_prog.clone(), ui.level_output_vol.clone()));
	
	let receiver = match Receiver::new(output.input_stream(), "0.0.0.0:7000") {
		Ok(ok) => ok,
		Err(e) => {
			println!("{}", e);
			return;
		}
	};
	
	let transmit_ = Arc::new(Mutex::new(false));
	let upload_speed_ = Arc::new(Mutex::new(0));

	let (udp_s, buffer_s) = net::udp::UdpSender::new(upload_speed_.clone());
	let input = Arc::new(Input::new(ui.level_input_prog.clone(), ui.level_input_vol.clone()));
	input.start(buffer_s);
	
	let _transmit_ = transmit_.clone();

	thread::spawn(move || {
		udp_s.send("0.0.0.0:6000", "127.0.0.1:7000", _transmit_);
	});
	
	let _transmit_ = transmit_.clone();
	
	ui.window.connect_key_press_event(move |_, event| {
		let key = event.get_keyval();
		
		if key == 65508 {
			*_transmit_.lock() = true;
		}
		
		gtk::Inhibit(false)
	});
	
	let _transmit_ = transmit_.clone();
	
	ui.window.connect_key_release_event(move |_, event| {
		let key = event.get_keyval();
		
		if key == 65508 {
			*_transmit_.lock() = false;
		}
		
		gtk::Inhibit(false)
	});
	
	let _settings_dialog = ui.settings_dialog.clone();;
	
	ui.menu_settings.connect_activate(move |_| {
		_settings_dialog.show_all();
	});
	
	let _input = input.clone();
	let _output = output.clone();
	let display_up = ui.display_up.clone();
	
	ui.do_every(move || {
		_input.update_level_prog();
		_output.update_level_prog();
	}, 20);
	
	ui.do_every(move || {
		let speed_i = {
			let mut up = upload_speed_.lock();
			let out = up.clone();
			*up = 0;
			out*4
		};
		
		let speed_f = speed_i as f32 / 1024_f32;
		let label_val = format!("U: {:.*} KiB/s", 1, speed_f);
		display_up.set_label(label_val.as_str());
	}, 250);
	
	ui.begin();
}

