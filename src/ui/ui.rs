use gtk::{self,WidgetExt};
use std::time::Instant;

pub struct UI {
	pub window: gtk::Window,
	pub level_input_prog: gtk::ProgressBar,
	pub level_input_vol: gtk::VolumeButton,
	pub level_output_prog: gtk::ProgressBar,
	pub level_output_vol: gtk::VolumeButton,
	pub text_input: gtk::Entry,
	pub text_send: gtk::Button,
	pub chat_list: gtk::ListBox,
	tickers: Vec<(Box<Fn()>, u64, Instant)>,
	pub settings_dialog: gtk::Dialog,
	pub menu_settings: gtk::MenuItem,
	pub display_up: gtk::Label,
}

impl UI {
	pub fn init() -> Self {
		gtk::init().unwrap();
		
		let builder = gtk::Builder::new_from_string(include_str!("layout.glade"));
		let window: gtk::Window = builder.get_object("app_window").unwrap();
		let settings_dialog: gtk::Dialog = builder.get_object("settings_dialog").unwrap();
		let level_input_progress: gtk::ProgressBar = builder.get_object("level_input_progress").unwrap();
		let level_input_volume: gtk::VolumeButton = builder.get_object("level_input_volume").unwrap();
		let level_output_prog: gtk::ProgressBar = builder.get_object("level_output_progress").unwrap();
		let level_output_vol: gtk::VolumeButton = builder.get_object("level_output_volume").unwrap();
		let _level_input_progress = level_input_progress.clone();
		let menu_settings: gtk::MenuItem = builder.get_object("menu_settings").unwrap();
		let display_up: gtk::Label = builder.get_object("display_up").unwrap();
		
		window.show_all();
		window.connect_delete_event(|_, _| {
			gtk::main_quit();
			gtk::Inhibit(false)
		});

		UI {
			window: window,
			level_input_prog: level_input_progress,
			level_input_vol: level_input_volume,
			level_output_prog: level_output_prog,
			level_output_vol: level_output_vol,
			text_input: builder.get_object("text_input").unwrap(),
			text_send: builder.get_object("text_send").unwrap(),
			chat_list: builder.get_object("chat_list").unwrap(),
			tickers: Vec::new(),
			settings_dialog: settings_dialog,
			menu_settings: menu_settings,
			display_up: display_up,
		}
	}
	
	pub fn do_every<F: Fn() + 'static>(&mut self, func: F, millis: u64) {
		self.tickers.push((Box::new(func), millis, Instant::now()));
	}
	
	pub fn begin(mut self) {
		gtk::idle_add(move || {
			let now = Instant::now();
		
			for &mut (ref func, ref millis, ref mut last) in &mut self.tickers {
				let duration = now.duration_since(last.clone());
				let elapsed = (duration.as_secs()*1000)+(duration.subsec_nanos()/1000000) as u64;
				
				if elapsed > *millis {
					*last = Instant::now();
					func();
				}
			}
			
			gtk::Continue(true)
		});
		
		gtk::main();
	}
}
