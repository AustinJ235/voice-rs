use gtk;
use gtk::ScaleButtonExt;
use std::sync::Arc;
use parking_lot::Mutex;
use net::stream;
use libpulse_sys::*;
use std::{mem,slice,ptr,thread};
use std::ffi::CString;
use std::time::{Instant,Duration};

const SEG_SIZE: usize = 960;
const SEG_AMOUNG: usize = 5;

pub struct Output {
	level_prog: gtk::ProgressBar,
	volume: gtk::VolumeButton,
	audio_level_: Arc<Mutex<f32>>,
	input_stream: Arc<stream::In>,
}

impl Output {
	pub fn new(level_prog: gtk::ProgressBar, volume: gtk::VolumeButton) -> Self {
		let audio_level_ = Arc::new(Mutex::new(0_f32));
		let _audio_level_ = audio_level_.clone();
		let input_stream = Arc::new(stream::In::new(SEG_SIZE, SEG_AMOUNG));
		let _input_stream = input_stream.clone();
		
		thread::spawn(move || {
			let audio_level_ = _audio_level_;
			let input_stream = _input_stream;
			
			let ss = pa_sample_spec {
				format: PA_SAMPLE_S16LE,
				channels: 1,
				rate: 48000
			};
			
			let buf = unsafe { mem::transmute(pa_xmalloc(2 * SEG_SIZE)) };
				
			'pulse: loop {
				let mut input_buf = input_stream.pop();
				println!("Starting Output");
			
				let s = unsafe {
					let name_c = CString::new("VOIP").unwrap();
					let desc_c = CString::new("Output").unwrap();
	
					pa_simple_new(ptr::null(),
						name_c.as_ptr() as *const i8,
						PA_STREAM_PLAYBACK,
						ptr::null(),
						desc_c.as_ptr() as *const i8,
						&ss,
						ptr::null(),
						ptr::null(),
						ptr::null_mut(),
					)
				}; assert!(s != ptr::null_mut());
				
				'write: loop {
					let samples: &mut [i16] = unsafe { slice::from_raw_parts_mut(buf, SEG_SIZE) };
					let mut highest = 0;
				
					for val in &input_buf {
						if val.abs() > highest {
							highest = val.abs();
						}
					}
				
					*audio_level_.lock() = highest as f32 / i16::max_value() as f32;

					for (i, sample) in samples.iter_mut().enumerate() {
						*sample = input_buf[i];
					}
	
					let res = unsafe { pa_simple_write(s, mem::transmute(buf), 2 * SEG_SIZE, ptr::null_mut()) };
					assert!(res == 0);
					
					'pop: for _ in 0..200 {
						match input_stream.try_pop() {
							Some(some) => {
								input_buf = some;
								continue 'write;
							}, None => {
								thread::sleep(Duration::from_millis(1));
								continue 'pop;
							}
						}
					} 
					
					println!("Stopping Output");
					unsafe { pa_simple_free(s) };
					continue 'pulse;	
				}
			}
		});
	
		Output {
			level_prog: level_prog,
			volume: volume,
			audio_level_: audio_level_,
			input_stream: input_stream,
		}
	}
	
	pub fn input_stream(&self) -> Arc<stream::In> {
		self.input_stream.clone()
	}

	pub fn update_level_prog(&self) {
		let volume = self.volume.get_value() as f32;
		self.level_prog.set_fraction(((*self.audio_level_.lock())*volume) as f64);
	}
}

