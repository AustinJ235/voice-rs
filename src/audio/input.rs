use gtk;
use alsa;
use alsa::pcm::{self,PCM};
use std::ffi::CString;
use std::thread;
use gtk::ScaleButtonExt;
use std::sync::mpsc;
use parking_lot::Mutex;
use std::sync::Arc;

pub struct Input {
	level_prog: gtk::ProgressBar,
	volume: gtk::VolumeButton,
	audio_level_: Arc<Mutex<f32>>,
}

impl Input {
	pub fn new(level_prog: gtk::ProgressBar, volume: gtk::VolumeButton) -> Self {
		Input {
			level_prog: level_prog,
			volume: volume,
			audio_level_: Arc::new(Mutex::new(0_f32)),
		}
	}
	
	pub fn start(&self, buffer_s: mpsc::Sender<Vec<i16>>) {
		let audio_level_ = self.audio_level_.clone();
		
		thread::spawn(move || {
			let pcm = PCM::open(&*CString::new("default").unwrap(), alsa::Direction::Capture, false).unwrap();
			let hwp = pcm::HwParams::any(&pcm).unwrap();
			
			hwp.set_channels(1).unwrap();
			hwp.set_rate(48000, alsa::ValueOr::Nearest).unwrap();
			hwp.set_format(pcm::Format::s16()).unwrap();
			hwp.set_access(pcm::Access::RWInterleaved).unwrap();
			pcm.hw_params(&hwp).unwrap();
			let io = pcm.io_i16().unwrap();
			
			loop {
				let mut buffer = Vec::new();
				buffer.resize(960, 0);
				
				let len = match io.readi(buffer.as_mut_slice()) {
					Ok(ok) => ok,
					Err(_) => break
				};
				
				buffer.truncate(len);
				let mut highest = 0_i16;
		
				for i in 0..len {
					if buffer[i].abs() > highest {
						highest = buffer[i].abs();
					}
				}
				
				let level = highest as f32 / i16::max_value() as f32;
				*audio_level_.lock() = level;
				
				if let Err(e) = buffer_s.send(buffer) {
					println!("Input failed to send to buffer: {}", e);
				}
			}
		});
	}

	pub fn update_level_prog(&self) {
		let volume = self.volume.get_value() as f32;
		self.level_prog.set_fraction(((*self.audio_level_.lock())*volume) as f64);
	}
}
